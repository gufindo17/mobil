<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $urlget = "http://localhost/api/index.php/mobil";
    private $urldelete = "http://localhost/api/index.php/mobil/delete";

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin', 'person');
    }

    public function mobil()
    {
        $this->load->view('dashboard_page/data_mobil');
    }

    public function getdata()
    {
        if($this->input->post('tablecari') != "" or $this->input->post('tablecari') != null)
        {
            $tabelcari = $this->input->post('tablecari');

        }else{
            $tabelcari ='';
        }
        $list = $this->person->get_data($tabelcari);

        $data = array();

        for ($i = 0; $i < sizeof($list); $i++) {
            $row = array();
            $row[] = $i + 1;
            $row[] = $list[$i]['no_rangka'];
            $row[] = $list[$i]['no_polisi'];
            $row[] = $list[$i]['merek'];
            $row[] = $list[$i]['tipe'];
            $row[] = $list[$i]['tahun'];
            $row[] = '<a style="display:block; margin:0px auto;margin-top:10px;" onclick="editdata(' . "'" . $list[$i]['id'] . "'" . ')" class="waves-effect waves-light btn blue">Edit</a><a style="display:block; margin:0px auto;margin-top:10px;" onclick="deletedata(' . "'" . $list[$i]['id'] . "'" . ')" class="waves-effect waves-light btn red">Delete</a>';

            $data[] = $row;
        }
        $out = array(
            "data" => $data,
        );
        echo json_encode($out);

    }

    public function deletedata()
    {
        $id = $this->input->post('id');

        $data = "id=" . $id;
        $curl = curl_init($this->urldelete);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        curl_close($curl);
        echo $result;
    }


}


