<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_data($tabelcari)
    {
        if ($tabelcari != '') {
            $this->db->where('merek', $tabelcari);
        }
        $this->db->order_by('id','desc');
        $db = $this->db->get('data_mobil');

        return $db->result_array();
    }
}