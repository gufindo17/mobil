<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Admin Page</title>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.min.css">
        <link href="//cdn.datatables.net/plug-ins/1.10.9/features/searchHighlight/dataTables.searchHighlight.css" rel="stylesheet" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css">
        <style type="text/css">
            body,html   { width:  100%;
                          height: 100%;}

                            
        </style>

        <script type= 'text/javascript'>
            $(document).ready(function () {
                var t = $('#example').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [[ 4,"desc" ]],
                    "columnDefs": [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                 }],
                    "ajax": { "url" :"http://localhost/litecash/index.php/Admin/cd_list" }       
                  });

                 $("#clik").click(function(){
                  swal({
                  title: 'Peringatan!',
                  text: "Apakah kamu yakin ingin keluar?",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes!'
            }).then(function () {
                location.href="http://localhost/litecash/index.php/login/logout"
            })
            })
            });

        </script>
        <style type="text/css">
                       .fadeops  {  animation :  examp 1.5s ease-in-out 1; }

            @keyframes examp {
                from {
                    opacity: 0;

                }

                to {
                    opacity: 1;
                }
            }
        </style>
    </head>
    <body>
  <div class="container-fill" style="width: 100%;">
   <div class="navbar navbar-inverse">
    <div class="navbar-header">
      <a href="http://localhost/litecash/index.php/Admin" class="navbar-brand">Dashboard</a>
    </div>
        <ul class="nav navbar-nav">
    
            <li><a href="<?php echo base_url(); ?>index.php/admin/user_tmoney"><span class="glyphicon glyphicon-user"></span> T-money user</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/admin/kulbig"><span class="glyphicon glyphicon-phone"></span> kulbig</a></li>
            
        </ul>

        <ul class="nav navbar-nav navbar-right">
                     <li><a id="clik"><span class="glyphicon glyphicon-log-out"></span> logout</a></li>

        </ul>
    </div>
  </div>

<div class="container-fill" style="width: 100%;  overflow: auto;">
     <table id="example" class="table-hover display fadeops" cellspacing="0" width="100%" style="height: 100%; width: 100%; border : 2px solid black; ">
            <thead>
                <tr>

                    <th>No</th>
                    <th>id</th>
                    <th>idTmoney</th>
                    <th>transaction</th>
                    <th>refNo</th>
                    <th>timeStamp</th>
                    <th>resultCode</th>
                    <th>resultDesc</th>
                    <th>productCode</th>
                    <th>productName</th>
                    <th>customerName</th>
                    <th>billNumber</th>
                    <th>billAmount</th>
                    <th>feeAmount</th>
                    <th>totalAmount</th>


                </tr>
            </thead>
 
            <tfoot>
                <tr>

                    <th>No</th>
                    <th>id</th>
                    <th>idTmoney</th>
                    <th>transaction</th>
                    <th>refNo</th>
                    <th>timeStamp</th>
                    <th>resultCode</th>
                    <th>resultDesc</th>
                    <th>productCode</th>
                    <th>productName</th>
                    <th>customerName</th>
                    <th>billNumber</th>
                    <th>billAmount</th>
                    <th>feeAmount</th>
                    <th>totalAmount</th>
                    
                </tr>
            </tfoot>
        </table>
    </body>
</html>
