<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Homepage</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .fadein   {  animation: example 2s ease-in-out 1;
              position: relative; }

        @keyframes example 
          {
            0%   { opacity: 0; top: 200px; background-color:white;}
            25%  { opacity:0.5; }
            50%  { opacity: 0.7;  }
            75%  { opacity: 0.9; }
            85%  { opacity: 0.9; }
            100% {left:0px; top:0px;}
          }

    </style>
    <script type="text/javascript">
       $(document).ready(function() {
  $("#clik").click(function(){
  swal({
  title: 'Peringatan!',
  text: "Apakah kamu yakin ingin keluar?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes!'
}).then(function () {
    location.href="http://www.litebig.com/litecash/index.php/login/logout"
})
})
  });
    </script>
  </head>

  <body>
<div class="container-fluid" style="margin-left: -15px;">
  <div class="navbar navbar-inverse">
    <div class="navbar-header">
      <a class="navbar-brand">LiteBIG</a>
    </div>
      
        <ul class="nav navbar-nav navbar-right">
            <li><a id="clik"><span class="glyphicon glyphicon-log-out"></span> logout</a></li>

    </div>
  </div>

    <div class="container">

 <div class="jumbotron fadein">
        <h1>Dashboard Admin</h1>
          <div class="row">
          <div class="col-md-3 col-lg-3">
          <p>
          <a class="btn btn-lg btn-primary" href="<?php echo base_url(); ?>index.php/admin/kulbig" role="button"><span class="glyphicon glyphicon-book" style="margin-right: 10px;"></span> Data Kulbig &raquo;</a>
          </p>
          </div>

           <div class="col-md-3 col-lg-3">
          <p>
          <a class="btn btn-lg btn-primary" href="<?php echo base_url(); ?>index.php/admin/bantuan" role="button"><span class="glyphicon glyphicon-user"></span> Data Bantuan&raquo;</a>
          </p>
          </div>

          </div>
          <div class="row">
          <div class="col-md-3 col-lg-3">
          <p>
          <!--<a class="btn btn-lg btn-primary" href="<?php echo base_url(); ?>index.php/admin/kulbig_2" role="button"><span class="glyphicon glyphicon-book" style="margin-right: 10px;"></span> Data Kulbig 2 &raquo;</a>-->
          </p>
          </div>
          </div>
          <!--<p>
          <a class="btn btn-lg btn-primary" href="< ?php echo base_url(); ?>index.php/admin/user_tmoney" role="button">View Data User &raquo;</a>
          </p> -->
          <!--<p>
          <a class="btn btn-lg btn-primary" href="< ?php echo base_url(); ?>index.php/admin/payment" role="button">View Payment &raquo;</a>
        </p> -->
      
  
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/jquery-3.2.1.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
