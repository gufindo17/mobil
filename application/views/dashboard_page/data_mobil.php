<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Mobil</title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/material-design-icons/iconfont/material-icons.css') ?>">
    <link href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js') ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/') ?>button.dataTables.min.css">
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>dataTables.button.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>button.flash.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>vfs_fonts.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>buttons.html5.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>ajaxfileupload.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


    <style type="text/css">


        btn {
            padding: 3px !important
        }

        .modal {
            z-index: 2005 !important;
        }


        body {
            background-color: #dedede;
        }


    </style>
</head>

<body>

<div class="container-fluid z-depth-1"
     style="background: white; display: block;margin: 50px auto; padding: 30px; overflow: auto; width: 90%;">
    <div>
        <select class="select" id="kriteria" onchange="reload_table();">
            <option value="">Pilih Kategori</option>
            <option value="Toyota">Toyota</option>
            <option value="Honda">Honda</option>
        </select>
    </div>
    <a onclick="insertdata();" style="margin-bottom: 15px; float: right" class="btn btn-blue"><i class="material-icons">add</i></a>
    <table id="user_list" class="table striped" width="1150px">
        <thead class="z-depth-1">
        <tr>
            <th>No</th>
            <th>Nomor Rangka</th>
            <th>Nomor Polisi</th>
            <th>Merek</th>
            <th>Tipe</th>
            <th>Tahun</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>

    </table>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('.modal').modal();
        $('.select').formSelect();

    });
    var usrlist = $("#user_list").DataTable({
        processing: !0,
        order: [],
        lengthMenu: [[10, 25, 50, 100, 150, 200, 300, 400, 500, 1000, -1], [10, 25, 50, 100, 150, 200, 300, 400, 500, 1000, "All"]],
        dom: 'lBfrtip',
        buttons: [
            {
                text: 'RELOAD',
                className: 'btn grey darken-1 waves-effect raes',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                action: function () {
                    reload_table();
                }
            },
        ],
        columnDefs: [
            {
                orderable: !1
            },
        ],

        ajax: {
            url: "<?php echo site_url('admin/getdata')?>",
            type: "POST",
            data: function (data) {
                data.tablecari = $('#kriteria').val();
            }
        },
    });


    function deletedata(a) {
        $.ajax({
            url: 'http://localhost/api/index.php/mobil/delete',
            type: 'POST',
            dataType: 'JSON',
            data: {id: a},
            success: function (data) {
                if (data.status == 'sukses') {
                    M.toast({html: 'sukses'});
                    reload_table();
                } else {
                    M.toast({html: 'gagal'});
                    reload_table();
                }

            }
        });
    }

    function editdata(a) {
        $.ajax({
            url: 'http://localhost/api/index.php/mobil/getbyid',
            type: 'POST',
            dataType: 'JSON',
            data: {id: a},
            success: function (data) {
                $("#no_rangka1").val(data.no_rangka);
                $("#no_polisi1").val(data.no_polisi);
                $("#merek1").val(data.merek);
                $("#tipe1").val(data.tipe);
                $("#tahun1").val(data.tahun);
                $("#id").val(data.id);
                $("#modal1").modal('open');
            }
        });
    }

    function editdataaft() {
        var no_polis = $("#no_polisi1").val();
        var no_rangka = $("#no_rangka1").val();
        var merek = $("#merek1").val();
        var tipe = $("#tipe1").val();
        var tahun = $("#tahun1").val();
        var id = $("#id").val();
        $.ajax({
            url: 'http://localhost/api/index.php/mobil/put',
            type: 'POST',
            dataType: 'JSON',
            data: {no_polisi: no_polis, id: id, no_rangka: no_rangka, merek: merek, tipe: tipe, tahun: tahun},
            success: function (d) {
                if (d.status == 'sukses') {
                    M.toast({html: 'sukses'});
                    $("#modal1").modal('close');
                    reload_table();
                } else {
                    M.toast({html: 'gagal'});
                    $("#modal1").modal('close');
                    reload_table();
                }
            }
        });
    }

    function insertdata() {
        $("#modal2").modal('open');
    }

    function insertdata1() {
        var no_polis = $("#no_polisi").val();
        var no_rangka = $("#no_rangka").val();
        var merek = $("#merek").val();
        var tipe = $("#tipe").val();
        var tahun = $("#tahun").val();

        $.ajax({
            url: 'http://localhost/api/index.php/mobil/post',
            type: 'POST',
            dataType: 'JSON',
            data: {no_polisi: no_polis, no_rangka: no_rangka, merek: merek, tipe: tipe, tahun: tahun},
            success: function (d) {
                if (d.status == 'sukses') {
                    M.toast({html: 'sukses'});
                    $("#modal2").modal('close');
                    reload_table();
                } else {
                    M.toast({html: 'gagal'});
                    $("#modal2").modal('close');
                    reload_table();
                }
            }
        });
    }

    function reload_table() {
        usrlist.ajax.reload(null, !1)
    }

</script>

</div>
</body>
<div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="row">
            <div id="body">
                <form id="userading" onsubmit="event.preventDefault(); insertdata1();">
                    <div class="input-field col s12">
                        <input placeholder="Nomor Rangka" id="no_rangka" name="no_rangka" type="text" class="validate">
                        <label>Nomor Polisi</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Nomor Polisi" id="no_polisi" name="no_rangka" type="text" class="validate">
                        <label>Nomor Poisi</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Merek" id="merek" name="merek" type="text" class="validate">
                        <label>Merek</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Tipe" id="tipe" name="tipe" type="text" class="validate">
                        <label>Tipe</label>
                    </div>
                    <div class="input-field col s12">
                        <select class="select" name="tahun" id="tahun">

                            <?php
                            $thn_skr = date('Y');
                            for ($x = $thn_skr; $x >= 1990; $x--) {
                                ?>
                                <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label>Tahun</label>
                    </div>
                    <button class="waves-effect waves-green btn" id="submit" type="submit" style="margin-top: 30px;">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="row">
            <div id="body">
                <form id="userading" onsubmit="event.preventDefault(); editdataaft();">
                    <div class="input-field col s12">
                        <input placeholder="Nomor Rangka" id="no_rangka1" name="no_rangka" type="text" class="validate">
                        <label>Nomor Polisi</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Nomor Polisi" id="no_polisi1" name="no_rangka" type="text" class="validate">
                        <label>Nomor Poisi</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Merek" id="merek1" name="merek" type="text" class="validate">
                        <label>Merek</label>
                    </div>
                    <div class="input-field col s12">
                        <input placeholder="Tipe" id="tipe1" name="tipe" type="text" class="validate">
                        <label>Tipe</label>
                    </div>
                    <div class="input-field col s12">
                        <select class="select" name="tahun" id="tahun1">
                            <?php
                            $thn_skr = date('Y');
                            for ($x = $thn_skr; $x >= 1990; $x--) {
                                ?>
                                <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label>Tahun</label>
                    </div>
                    <input type="hidden" id="id">
                    <button class="waves-effect waves-green btn" id="submit" type="submit" style="margin-top: 30px;">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
</html>


