<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobil extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mobil_model');
	}

	public function postdata()
	{
		$rangka=$this->input->post('no_rangka');
		$polisi=$this->input->post('no_polisi');
		$merek=$this->input->post('merek');
		$tipe=$this->input->post('tipe');
		$tahun=$this->input->post('tahun');

		$data=array(
			'no_rangka'=>$rangka,
			'no_polisi'=>$polisi,
			'merek'=>$merek,
			'tipe'=>$tipe,
			'tahun'=>$tahun
		);

		$db=$this->Mobil_model->postdata($data);

		if ($db=='sukses'){
			echo json_encode(array('status'=>'sukses'));
		}else{
			echo json_encode(array('status'=>'gagal'));
		}
	}

	public function getbyid()
	{
		$id=$this->input->post('id');
		$db=$this->Mobil_model->getbyid($id);

		echo json_encode($db);
	}

	public function get()
	{
		$db=$this->Mobil_model->get();

		echo json_encode($db);
	}

	public function delete()
	{
        $id=$this->input->post('id');
		$db=$this->Mobil_model->delete($id);

		if ($db=='sukses'){
			echo json_encode(array('status'=>'sukses'));
		}else{
			echo json_encode(array('status'=>'gagal'));
		}
	}

	public function put()
	{
		$id=$this->input->post('id');
		$rangka=$this->input->post('no_rangka');
		$polisi=$this->input->post('no_polisi');
		$merek=$this->input->post('merek');
		$tipe=$this->input->post('tipe');
		$tahun=$this->input->post('tahun');

		$data=array(
			'no_rangka'=>$rangka,
			'no_polisi'=>$polisi,
			'merek'=>$merek,
			'tipe'=>$tipe,
			'tahun'=>$tahun
		);
		$db=$this->Mobil_model->put($id,$data);

		if ($db=='sukses'){
			echo json_encode(array('status'=>'sukses'));
		}else{
			echo json_encode(array('status'=>'gagal'));
		}
	}



}
