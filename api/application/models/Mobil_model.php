<?php

class Mobil_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database("default");
	}

	public function postdata($data)
	{
		$db = $this->db->insert('data_mobil', $data);

		if ($db) {
			return 'sukses';
		} else {
			return 'gagal';
		}
	}

	public function get()
	{
		$this->db->order_by('id', 'desc');
		$db = $this->db->get('data_mobil');
		return $db->result_array();
	}

	public function getbyid($id)
	{
		$this->db->where('id', $id);
		$db = $this->db->get('data_mobil');
		$dbx = $db->result_array();

		for ($i = 0; $i < sizeof($dbx); $i++) {
			return $dbx[$i];
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$db = $this->db->delete('data_mobil');

		if ($db) {
			return 'sukses';
		} else {
			return 'gagal';
		}
	}

	public function put($id, $data)
	{
		$this->db->where('id', $id);
		$db = $this->db->update('data_mobil', $data);

		if ($db) {
			return 'sukses';
		} else {
			return 'gagal';
		}
	}


}
