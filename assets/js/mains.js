;(function () {
	

	'use strict';

	// Placeholder 
	var placeholderFunction = function() {
		$('input, textarea').placeholder({ customClass: 'my-placeholder' });
	}
	
	// Placeholder 
	var contentWayPoint = function() {
		var i = 0;
		$('.animate-box').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('animated-fast') ) {
				
				i++;

				$(this.element).addClass('item-animate');
				setTimeout(function(){

					$('body .animate-box.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn animated-fast');
							} else if ( effect === 'fadeInLeft') {
								el.addClass('fadeInLeft animated-fast');
							} else if ( effect === 'fadeInRight') {
								el.addClass('fadeInRight animated-fast');
							} else {
								el.addClass('fadeInUp animated-fast');
							}

							el.removeClass('item-animate');
						},  k * 200, 'easeInOutExpo' );
					});
					
				}, 100);
				
			}

		} , { offset: '85%' } );
	};
	// On load
	$(function(){
		placeholderFunction();
		contentWayPoint();

	});

}());



  $(document).ready(function() {
    $("#forming").submit(function(e) {
        e.preventDefault();
        var name = $("#username").val();
        var pass = $("#password").val();

        $.ajax({

          url: '<?php echo base_url('index.php/login/Aksi_login') ?>',
          type: 'POST',
          data: {username: name,password: pass},
          beforeSend : function  () {
            $("#loginModal").modal("show");
          },
          success : function  (data) {
             $("#loginModal").modal("hide");
            if (data == "sukses") {
              location.href="<?php echo base_url('index.php/dashboard'); ?>"
            }
            else {      
        swal({
                    title: 'Gagal',
                    timer: 2000,
                    type:'error',
                    allowOutsideClick: false,
                    html: $('<div>')
                    .addClass('some-class')
                    .text('Sepertinya password atau username anda salah..!'),
                    animation: false,
                    customClass: 'animated shake',
                  }).then(
                    function () { $('#forming')[0].reset(); },
                    // handling the promise rejection
                    function (dismiss) {
                      if (dismiss === 'timer') {
                        console.log('gagal');
                        $('#forming')[0].reset();
                      }
                    }
                  )
            }
          }
        });     
    });
  });
